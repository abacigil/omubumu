# FlightRecorder SDK

[![Version](https://img.shields.io/cocoapods/v/FlightRecorder.svg?style=flat)](http://cocoadocs.org/docsets/FlightRecorder)
[![License](https://img.shields.io/cocoapods/l/FlightRecorder.svg?style=flat)](http://cocoadocs.org/docsets/FlightRecorder)
[![Platform](https://img.shields.io/cocoapods/p/FlightRecorder.svg?style=flat)](http://cocoadocs.org/docsets/FlightRecorder)

## Usage

Quick Start Guide : http://help.flightrecorder.co/quick-start/

## Requirements

Please sign-up for free to try FlightRecorder. You will need an access and a secret key.

Sign-up Free : http://flightrecorder.co/Account/Signup

Pro-Packages : http://www.flightrecorder.co/#price-list

Quick Start Guide : http://help.flightrecorder.co/quick-start/

Full Documentation : http://help.flightrecorder.co/

## Installation

FlightRecorder is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "FlightRecorder"

## Author

FlightRecorder, support@flightrecorder.co

## License

FlightRecorder is available under the Commercial license. See the LICENSE file for more info and visit http://www.flightrecorder.co/home/tos

