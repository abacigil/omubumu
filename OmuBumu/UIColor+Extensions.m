//
//  UIColor+HexColor.m
//  MekanistiOS
//
//  Created by Eren Baydemir on 26.05.2012.
//  Copyright (c) 2012 Mekanist.net. All rights reserved.
//

#import "UIColor+Extensions.h"

@implementation UIColor (HexColor)

+ (UIColor *)kudretBlue
{
    return [UIColor colorWithRed:74.0/255.0 green:98.0/255.0 blue:121.0/255.0 alpha:1];
}

+ (UIColor *) clickableBlue
{
    return [UIColor colorWithHEX:@"#1966CC"];
}

+ (UIColor *) clickableBlueHighlighted
{
    return [UIColor colorWithHEX:@"#1966CC55"];
}

+ (UIColor *) cellBorder
{
    return [UIColor colorWithHEX:@"#CFCFCF"];
}

+ (UIColor *) cellDarkBorder
{
    return [UIColor colorWithHEX:@"#888888"];
}

+ (UIColor*) colorWithHEX:(NSString*)hexString
{
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    if (hexString.length != 6 && hexString.length != 8)
        return [UIColor blackColor];
    
    BOOL hasAlpha = NO;
    if (hexString.length == 8)
        hasAlpha = YES;
    
    NSString *Hred = [hexString substringToIndex:2];
    hexString = [hexString substringFromIndex:2];
    NSString *Hgreen = [hexString substringToIndex:2];
    hexString = [hexString substringFromIndex:2];
    NSString *Hblue = [hexString substringToIndex:2];
    NSString *Halpha = @"";
    
    int alpha = 255;
    
    if (hasAlpha)
    {
        hexString = [hexString substringFromIndex:2];
        Halpha = hexString;
        alpha = [self convertHEXtoINT:Halpha];
    }
    
    int red = [self convertHEXtoINT:Hred];
    int green = [self convertHEXtoINT:Hgreen];
    int blue = [self convertHEXtoINT:Hblue];
    
    return [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha/255.0];
}

+ (unsigned) convertHEXtoINT:(NSString*)hex
{
    unsigned result = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hex];
    
    //[scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&result];
    return result;
}
@end
