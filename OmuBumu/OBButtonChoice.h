//
//  OBButtonChoice.h
//  OmuBumu
//
//  Created by Davut Can Abacigil on 02/07/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OBButtonChoice : UIButton
@property(nonatomic, strong) NSString *QuestionID;
@property(nonatomic, strong) NSString *ChoiceID;
@end
