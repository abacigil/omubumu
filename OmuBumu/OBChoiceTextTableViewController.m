//
//  OBChoiceTextTableViewController.m
//  OmuBumu
//
//  Created by Davut Can Abacigil on 01/07/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import "OBChoiceTextTableViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MBProgressHUD/MBProgressHUD.h>
@interface OBChoiceTextTableViewController ()

@end

@implementation OBChoiceTextTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.QuestionType = [[self.selectedQuestionType objectForKey:@"Type"] intValue];
    
    if(self.QuestionType == 200)
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Devam" style:UIBarButtonItemStylePlain target:self action:@selector(openCamera:)];
        
       self.picker = [[UIImagePickerController alloc] init];
       self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
       self.picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:
                                 UIImagePickerControllerSourceTypeCamera];
       self.picker.delegate = self;
    }
    else
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Devam" style:UIBarButtonItemStylePlain target:self action:@selector(sendQuestionToServer)];
    }
    
    
    
    
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.QuestionType == 200)
    {

        return 1;
    }
    else if(self.QuestionType == 100)
    {
        return 3;
    }
    else
    {
        return 6;
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    NSString *mediaTypeInfo = @"";
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    if(CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeImage, 0)
       == kCFCompareEqualTo)
    {
        UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
        
        UIImage *smallImage = [self imageScaleWithValue:700 originalImage:chosenImage];
        
        NSData *imageData = UIImageJPEGRepresentation(smallImage, 0.7);
        self.takenPicture = imageData;
        
        mediaTypeInfo = @"Photo";
        
    }
    else
    {
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
        
        self.videoURL = videoData;
        
        mediaTypeInfo = @"Video";
        
    }
    
    //[[FlightRecorder sharedInstance] trackEventWithCategory:@"Camera" action:@"Use Photo" label:@"User approved media" value:mediaTypeInfo];
    
    [self sendQuestionToServer];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //[[FlightRecorder sharedInstance] trackEventWithCategory:@"Camera" action:@"Cancel" label:@"User tapped on Cancel" value:nil];
    //[[FlightRecorder sharedInstance] warningWithName:@"Camera" value:@"Cancel"];
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0)
    {
        //[[FlightRecorder sharedInstance] trackEventWithCategory:@"Flow Action" action:@"Media Selected" label:@"Open Camera" value:@"Camera"];
        
        self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.picker animated:YES completion:nil];
    }
    else if(buttonIndex == 1)
    {
        //[[FlightRecorder sharedInstance] trackEventWithCategory:@"Flow Action" action:@"Media Selected" label:@"Open Camera" value:@"PhotoLib"];
        
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.picker animated:YES completion:nil];
    }
    
    
}

- (IBAction)openCamera:(id)sender {
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Kaynak" delegate:self cancelButtonTitle:@"İptal" destructiveButtonTitle:nil otherButtonTitles:@"Kamera",@"Kütüphane", nil];
    
    [sheet showInView:self.view];
    
    
    [[FlightRecorder sharedInstance] trackPageView:@"Camera View"];
    
    
    
    
}



-(void)sendChoicesToServer:(PFObject *)question
{
   

    if(self.QuestionType == 200)
    {
        PFObject *mediaQuestion = [PFObject objectWithClassName:@"Choices"];
        [mediaQuestion setObject:[PFUser currentUser] forKey:@"user"];
        [mediaQuestion setObject:question forKey:@"Question"];
        
        [mediaQuestion setObject:[NSNumber numberWithInt:1] forKey:@"IsMedia"];
        PFFile *file;
        if (self.takenPicture == nil)
        {
            file = [PFFile fileWithName:@"video.mov" data:self.videoURL];
            [mediaQuestion setObject:[NSNumber numberWithInt:2] forKey:@"MediaType"];
        }
        else
        {
            file = [PFFile fileWithName:@"image.jpg" data:self.takenPicture];
            [mediaQuestion setObject:[NSNumber numberWithInt:1] forKey:@"MediaType"];
        }
        
        mediaQuestion[@"Media"] = file;
        
        
        [mediaQuestion saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
          
                PFObject *point = [PFObject objectWithoutDataWithClassName:@"AskedQuestion" objectId:question.objectId];
                
                
            [point setObject:mediaQuestion forKey:@"Choice1"];
                
                // Save
                [point save];
                
                
            

            
            self.videoURL = nil;
            self.takenPicture = nil;
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];

    }
    else
    {
        NSMutableArray *choices = [NSMutableArray array];
        
        [choices addObject:self.textField1.text];
        [choices addObject:self.textField2.text];
        if(self.textField3 != nil && ![self.textField3.text isEqualToString:@""])
            [choices addObject:self.textField3.text];
        if(self.textField4 != nil && ![self.textField4.text isEqualToString:@""])
            [choices addObject:self.textField4.text];
        if(self.textField5 != nil && ![self.textField5.text isEqualToString:@""])
            [choices addObject:self.textField5.text];
        
        
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            int i = 1;
            for (NSString *c in choices) {
                if(![c isEqualToString:@""])
                {
                    PFObject *textQuestion = [PFObject objectWithClassName:@"Choices"];
                    [textQuestion setObject:[PFUser currentUser] forKey:@"user"];
                    [textQuestion setObject:question forKey:@"Question"];
                    [textQuestion setObject:[NSNumber numberWithInt:0] forKey:@"IsMedia"];
                 
                    [textQuestion setObject:c forKey:@"Choice"];
                    
                    BOOL result = [textQuestion save];
                    
                    NSString *choiceFiled = [NSString stringWithFormat:@"Choice%i", i];
                    if (result) {
                        
                        PFObject *point = [PFObject objectWithoutDataWithClassName:@"AskedQuestion" objectId:question.objectId];
                        
                    
                        [point setObject:textQuestion forKey:choiceFiled];
                        
                        // Save
                        [point save];
                        
                    
                    }
                    
                    i++;
                }
               
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            });
            
        });

        
        
        
    }
  
        
        
}

-(BOOL)checkAnswers
{
    if(self.QuestionType == 200)
    {
        if(self.videoURL == nil && self.takenPicture == nil)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Uyarı" message:@"Fotoğraf veya video yüklemeniz gerekli." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
            [alert show];
            
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        if([self.textField1.text isEqualToString:@""])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Uyarı" message:@"İlk cevap alanı boş olamaz." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
            [alert show];
            
            return NO;
        }
        
        if([self.textField2.text isEqualToString:@""])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Uyarı" message:@"En az 3 cevap girmeniz gerekli." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
            [alert show];
            
            return NO;
        }
        
        if(self.QuestionType == 101 && [self.textField3.text isEqualToString:@""])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Uyarı" message:@"En az 3 cevap girmeniz gerekli." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
            [alert show];
            
            return NO;
        }
        
        return YES;

    }
    
    return YES;
}


-(void)sendQuestionToServer
{
    if(self.textViewQuestion.text.length < 5)
    {
#warning mesaji doldur
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"5 karakter" message:@"" delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    BOOL result = [self checkAnswers];
    
    if(result == NO)
        return;
    
    PFObject *question = [PFObject objectWithClassName:@"AskedQuestion"];
    [question setObject:[PFUser currentUser] forKey:@"user"];
    [question setObject:self.textViewQuestion.text forKey:@"Question"];
    [question setObject:self.selectedQuestionType forKey:@"QuestionType"];
    [question setObject:[NSNumber numberWithBool:YES] forKey:@"IsApproved"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        BOOL saveResult = [question save];
        
        if(saveResult)
        {
            [self sendChoicesToServer:question];
        }
        else{
#warning fail message
        }
    });
    
    
    
    
    
}




#pragma mark -
#pragma mark UIImage Scale
-(UIImage*)imageScaleWithImage: (UIImage*) sourceImage percent: (float) percent
{
    
    
    float newWidth = sourceImage.size.width * percent / 100;
    float newHeight = sourceImage.size.height * percent / 100;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToHeight: (float) i_height
{
    float oldHeight = sourceImage.size.height;
    float scaleFactor = i_height / oldHeight;
    
    float newWidth = sourceImage.size.width * scaleFactor;
    float newHeight = oldHeight * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage *)imageScaleWithValue:(CGFloat)maxValue originalImage:(UIImage *)img
{
    UIImage *imgSized;
    if(img.size.width < img.size.height)
    {
        
        if(img.size.height > maxValue)
            imgSized = [self imageWithImage:img scaledToHeight:maxValue];
        else {
            imgSized = img;
        }
        
        return imgSized;
    }
    else if (img.size.width > img.size.height ){
        
        if(img.size.width > maxValue)
            imgSized = [self imageWithImage:img scaledToWidth:maxValue];
        else {
            imgSized = img;
        }
        return imgSized;
    }
    else {
        
        if(img.size.height > maxValue)
            imgSized = [self imageWithImage:img scaledToHeight:maxValue];
        else {
            imgSized = img;
        }
        
        return imgSized;
        
    }
    
    
    
}



@end
