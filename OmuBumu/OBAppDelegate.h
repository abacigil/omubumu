//
//  OBAppDelegate.h
//  OmuBumu
//
//  Created by Davut Can Abacigil on 01/07/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
