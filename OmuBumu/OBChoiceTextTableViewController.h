//
//  OBChoiceTextTableViewController.h
//  OmuBumu
//
//  Created by Davut Can Abacigil on 01/07/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OBChoiceTextTableViewController : UITableViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UITextFieldDelegate>
@property(nonatomic, strong) PFObject *selectedQuestionType;
@property(nonatomic, strong) UIImagePickerController *picker;
@property(nonatomic, strong) NSData *takenPicture;
@property(nonatomic, strong) NSData *videoURL;
@property(nonatomic, assign) int QuestionType;
@property (weak, nonatomic) IBOutlet UITextView *textViewQuestion;
@property (weak, nonatomic) IBOutlet UITextField *textField1;
@property (weak, nonatomic) IBOutlet UITextField *textField2;
@property (weak, nonatomic) IBOutlet UITextField *textField3;
@property (weak, nonatomic) IBOutlet UITextField *textField4;
@property (weak, nonatomic) IBOutlet UITextField *textField5;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellAnswer3;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellAnswer4;
@property (weak, nonatomic) IBOutlet UITableViewCell *cellAnswer5;
@end
