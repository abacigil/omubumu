//
//  OBAppDelegate.m
//  OmuBumu
//
//  Created by Davut Can Abacigil on 01/07/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import "OBAppDelegate.h"

@implementation OBAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [Parse setApplicationId:@"MAnkiQhAsITXPZhDKBB9vFYbLu607uPQjjPV7D3P"
                  clientKey:@"4n5XO1pV8tLopISFVScl0WGrvNiKV94i7w5h0QjR"];
    
    [PFFacebookUtils initializeFacebook];
    [PFTwitterUtils initializeWithConsumerKey:@"JeVQM9NNS4hN4VEut58hzR3F7" consumerSecret:@"BIh6CoMT2tby3SWbCxg1e3B9pZUhZGKKvJ2c5MYACrazpFd3UR"];
    
    // Set default ACLs
    PFACL *defaultACL = [PFACL ACL];
    [defaultACL setPublicReadAccess:YES];
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];

    
   

    
    // Override point for customization after application launch.
    return YES;
}


// Facebook oauth callback
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    //[[FlightRecorder sharedInstance] trackEventWithCategory:@"Login" action:@"Facebook" label:@"From Safari" value:nil];
    
    return [PFFacebookUtils handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    //[[FlightRecorder sharedInstance] trackEventWithCategory:@"Login" action:@"Facebook" label:@"From App" value:sourceApplication];
    
    return [PFFacebookUtils handleOpenURL:url];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Handle an interruption during the authorization flow, such as the user clicking the home button.
    [FBSession.activeSession handleDidBecomeActive];
    
    
//    if ([PFUser currentUser]) {
//        
//        if([PFUser currentUser].username != nil)
//        {
//            [[FlightRecorder sharedInstance] setSessionUserID:[PFUser currentUser].username];
//        }
//        else
//        {
//            [[FlightRecorder sharedInstance] setSessionUserID:[PFUser currentUser].email];
//        }
//        
//    }
}

							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
