//
//  OBNewQuestionTableViewController.m
//  OmuBumu
//
//  Created by Davut Can Abacigil on 01/07/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import "OBNewQuestionTableViewController.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "OBChoiceTextTableViewController.h"
@interface OBNewQuestionTableViewController ()

@end

@implementation OBNewQuestionTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.title = @"Soru Tipi";
    self.questionTypes = [NSMutableArray array];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PFQuery *query = [PFQuery queryWithClassName:@"QuestionTypes"];
    [query orderByAscending:@"Order"];
    [query whereKey:@"IsActive" equalTo:[NSNumber numberWithInt:1]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            
            [self.questionTypes addObjectsFromArray:objects];
            
            [self.tableView reloadData];
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
        } else {
            NSString *errorStr = [NSString stringWithFormat:@"Error: %@ %@", error, [error userInfo]];
            [[FlightRecorder sharedInstance] logAPIRequestWithName:@"Load Belediyeler" url:@"Belediyeler" httpMethod:@"GET" requestBody:nil requestHeaders:nil responseStatusCode:500 responseString:errorStr responseHeaders:nil];
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];

    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.questionTypes.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 142;
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    PFObject *object = [self.questionTypes objectAtIndex:indexPath.row];
    
    UITableViewCell *cell;
    
    
    
    cell = [self.tableView dequeueReusableCellWithIdentifier:@"cellQuestion"];
    
    
    UILabel *btn = (UILabel *)[cell viewWithTag:101];
    btn.text = [object objectForKey:@"Name"];
    
    
    
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:100];
    [imgView setImageWithURL:[NSURL URLWithString:[object objectForKey:@"Logo"]]];
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[[FlightRecorder sharedInstance] trackEventWithCategory:@"UI Actions" action:@"TableView Row" label:@"Belediye Selected" value:[self.selectedBelediye objectForKey:@"belediyeAdi"]];
    
    PFObject *object = [self.questionTypes objectAtIndex:indexPath.row];
    
    
        OBChoiceTextTableViewController *choice = [self.storyboard instantiateViewControllerWithIdentifier:@"OBChoiceTextTableViewController"];
        
        choice.selectedQuestionType = object;
        
        [self.navigationController pushViewController:choice animated:YES];
   
    
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
