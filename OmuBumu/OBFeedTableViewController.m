//
//  OBFeedTableViewController.m
//  OmuBumu
//
//  Created by Davut Can Abacigil on 01/07/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import "OBFeedTableViewController.h"
#import "UIView+UpdateAutoLayoutConstraints.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface OBFeedTableViewController ()

@end

@implementation OBFeedTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (PFQuery *)queryForTable {
    
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    
    if (self.feedType == FEED_TYPE_NEW) {

      
        if (self.objects.count == 0) {
            query.cachePolicy = kPFCachePolicyCacheThenNetwork;
        }
        
        [query whereKey:@"IsApproved" equalTo:[NSNumber numberWithBool:YES]];
        [query includeKey:@"QuestionType"];
        
        for (int i = 1; i < 6; i++) {
            
            NSString *choiceObject= [NSString stringWithFormat:@"Choice%i",i];

            [query includeKey:choiceObject];

        }
        
        
        [query orderByDescending:@"createdAt"];
       
    }
    
   
    
    return query;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
                        object:(PFObject *)object
{
    PFObject *questionType = [object objectForKey:@"QuestionType"];
    
    if([[questionType objectForKey:@"Type"] intValue] == 200)
    {
        UITableViewCell *cellPhoto = [tableView dequeueReusableCellWithIdentifier:@"cellPhoto"];
        
        UILabel *labelTitle = (UILabel *)[cellPhoto viewWithTag:600];
        
        labelTitle.text = [object objectForKey:@"Question"];
        
        PFObject *choice = [object objectForKey:@"Choice1"];
        PFFile *file = [choice objectForKey:@"Media"];
        UIImageView *imgView = (UIImageView *)[cellPhoto viewWithTag:601];
        [imgView setImageWithURL:[NSURL URLWithString:file.url]];
        
        return  cellPhoto;
        
    }
    
    
    
    UITableViewCell *cellText = [tableView dequeueReusableCellWithIdentifier:@"cellTextChoice"];
    
    UILabel *labelTitle = (UILabel *)[cellText viewWithTag:100];
    
    labelTitle.text = [object objectForKey:@"Question"];
    
    UIView *choiceContainerView = (UIView *)[cellText viewWithTag:101];
    choiceContainerView.clipsToBounds = YES;
    float yPoint = 0;
    
    
    
    int countChoice = 0;
    for (int i = 1; i < 6; i++) {
        
        
        NSString *choiceObjectString = [NSString stringWithFormat:@"Choice%i",i];
        
        if([object objectForKey:choiceObjectString] != nil)
        {
            countChoice++;
            
        }
    }
    
    for (UIView *V in choiceContainerView.subviews) {
        if([V isKindOfClass:[UIButton class]])
        {
            [V removeFromSuperview];
        }
    }
    
    float btnHeight = 200/countChoice;
    float btnFontSize = 20;
    for (int i = 1; i < 6; i++) {
        
        
        NSString *choiceObjectString = [NSString stringWithFormat:@"Choice%i",i];
        
        if([object objectForKey:choiceObjectString] != nil)
        {
            PFObject *choiceObject = [object objectForKey:choiceObjectString];
            OBButtonChoice *btn = [OBButtonChoice buttonWithType:UIButtonTypeRoundedRect];
            btn.frame = CGRectMake(10, yPoint, 285, btnHeight);
            [btn setTitle:[choiceObject objectForKey:@"Choice"] forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor yellowColor]];
            [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:btnFontSize]];
            btn.ChoiceID = choiceObject.objectId;
            btn.QuestionID = object.objectId;
            [btn addTarget:self action:@selector(answerQuestion:) forControlEvents:UIControlEventTouchUpInside];
            
            
          
            
            for (PFObject *userAnswer in self.userAnswers) {
                PFObject *userAnswerChoice = [userAnswer objectForKey:@"Choice"];
                NSString *userAnswerChoiceID = userAnswerChoice.objectId;
                if([choiceObject.objectId isEqualToString:userAnswerChoiceID])
                {
                    btn.backgroundColor = [UIColor redColor];
                    btn.userInteractionEnabled = NO;
                }
            }
            
            
            [choiceContainerView addSubview:btn];
            
            
            yPoint += btnHeight + 5;
            
            
        }
      
        
    }
    
    return cellText;

    
}


-(void)answerQuestion:(OBButtonChoice *)sender
{
    PFObject *question = [PFObject objectWithoutDataWithClassName:@"AskedQuestion" objectId:sender.QuestionID];
    PFObject *choice = [PFObject objectWithoutDataWithClassName:@"Choices" objectId:sender.ChoiceID];
    
    PFQuery *deleteUserPreviousAnswer = [PFQuery queryWithClassName:@"Answers"];
    
    [deleteUserPreviousAnswer whereKey:@"Question" equalTo:question];
    [deleteUserPreviousAnswer whereKey:@"user" equalTo:[PFUser currentUser]];
    
    [deleteUserPreviousAnswer findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if(objects == nil || objects.count == 0)
        {
            PFObject *answer = [PFObject objectWithClassName:@"Answers"];
            [answer setObject:[PFUser currentUser] forKey:@"user"];
            [answer setObject:question forKey:@"Question"];
            [answer setObject:choice forKey:@"Choice"];
            [answer saveInBackground];
            
            [self loadUserAnswers];
        }
        
        for (PFObject *ans in objects) {
            [ans deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                
                PFObject *answer = [PFObject objectWithClassName:@"Answers"];
                [answer setObject:[PFUser currentUser] forKey:@"user"];
                [answer setObject:question forKey:@"Question"];
                [answer setObject:choice forKey:@"Choice"];
                [answer saveInBackground];
                
                [self loadUserAnswers];
                
            }];
        }
        
        
    }];
    
    
   
    
   
    
    
    
}

-(void)loadUserAnswers
{
    self.userAnswers = [NSMutableArray array];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        for (int i =0; i<self.objects.count; i++) {
            PFObject *question = [self.objects objectAtIndex:i];
            PFQuery *answers = [PFQuery queryWithClassName:@"Answers"];
            [answers whereKey:@"Question" equalTo:question];
            [answers whereKey:@"user" equalTo:[PFUser currentUser]];
            
            NSArray *arr = [answers findObjects];
            
            [self.userAnswers addObjectsFromArray:arr];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tableView reloadData];
        });
        
    });
}

-(void)objectsDidLoad:(NSError *)error
{
    
    
    [self loadUserAnswers];
    
    
    [super objectsDidLoad:error];
}

-(void)viewDidAppear:(BOOL)animated
{
    // Check if user is logged in
    if (![PFUser currentUser]) {
        // If not logged in, we will show a PFLogInViewController
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        
        
        // Customize the Log In View Controller
        logInViewController.delegate = self;
        logInViewController.facebookPermissions = @[@"email"];
        logInViewController.fields =  PFLogInFieldsFacebook | PFLogInFieldsTwitter | PFLogInFieldsLogInButton | PFLogInFieldsSignUpButton | PFLogInFieldsUsernameAndPassword ;
        // Show Twitter login, Facebook login, and a Dismiss button.
        
        //logInViewController.logInView.logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
        
        logInViewController.signUpController.signUpView.emailField.placeholder = @"E-posta adresin";
        logInViewController.signUpController.signUpView.passwordField.placeholder = @"Şifren";
        logInViewController.signUpController.signUpView.usernameField.placeholder = @"Kullanıcı adın";
        //logInViewController.signUpController.signUpView.logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
        [logInViewController.signUpController.signUpView.signUpButton setTitle:@"Kayıt Ol" forState:UIControlStateNormal];
        
        
        [logInViewController.logInView.signUpButton setTitle:@"Kayıt Ol" forState:UIControlStateNormal];
        logInViewController.logInView.passwordField.placeholder = @"Şifren";
        logInViewController.logInView.usernameField.placeholder = @"Kullanıcı adın";
        [logInViewController.logInView.passwordForgottenButton setTitle:@"Unuttum" forState:UIControlStateNormal];
        logInViewController.logInView.externalLogInLabel.text = @"Facebook veya Twitter ile giriş yap";
        logInViewController.logInView.signUpLabel.text = @"Kayıt olmak mı istiyorsun?";
        
        // Present Log In View Controller
        [self presentViewController:logInViewController animated:YES completion:^{
            
            
            
        }];
        
        
        // [[FlightRecorder sharedInstance] trackPageView:@"Login View"];
        
        
        
    }
    else
    {
        //[[FlightRecorder sharedInstance] trackPageView:@"Report View"];
    }
    
    [super viewDidAppear:animated];
}


#pragma mark - PFLogInViewControllerDelegate

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    
    if ([PFUser currentUser]) {
        
        if([PFUser currentUser].username != nil)
        {
            //            [[FlightRecorder sharedInstance] setSessionUserID:[PFUser currentUser].username];
            //            [[FlightRecorder sharedInstance] logWithName:@"Login Success" value:[PFUser currentUser].username];
        }
        else
        {
            //            [[FlightRecorder sharedInstance] setSessionUserID:[PFUser currentUser].email];
            //            [[FlightRecorder sharedInstance] logWithName:@"Login Success" value:[PFUser currentUser].email];
        }
        
    }
    
    
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    
    if(error != nil)
    {
        //        [[FlightRecorder sharedInstance] warningWithName:@"Login Fail" value:error.description];
        //
        //        [[FlightRecorder sharedInstance] trackEventWithCategory:@"Login" action:@"Error" label:@"System Error" value:error.description];
    }
    else
    {
        //        [[FlightRecorder sharedInstance] warningWithName:@"Login Fail" value:@"No description"];
        //
        //        [[FlightRecorder sharedInstance] trackEventWithCategory:@"Login" action:@"Error" label:@"System Error" value:@"No description"];
    }
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    NSLog(@"User dismissed the logInViewController");
}



- (void)viewDidLoad
{
   
    self.feedType = FEED_TYPE_NEW;
    self.parseClassName = @"AskedQuestion";
    self.pullToRefreshEnabled = YES;
    self.paginationEnabled = YES;
    self.objectsPerPage = 25;
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
