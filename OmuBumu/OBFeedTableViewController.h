//
//  OBFeedTableViewController.h
//  OmuBumu
//
//  Created by Davut Can Abacigil on 01/07/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBButtonChoice.h"
typedef enum FEED_TYPE{
    FEED_TYPE_POPULAR,
    FEED_TYPE_NEW
}FEED_TYPE;
@interface OBFeedTableViewController : PFQueryTableViewController
@property(nonatomic, assign) FEED_TYPE feedType;
@property(nonatomic, strong) NSMutableArray *userAnswers;
@end
