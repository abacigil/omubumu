//
//  OBNewQuestionTableViewController.h
//  OmuBumu
//
//  Created by Davut Can Abacigil on 01/07/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OBNewQuestionTableViewController : UITableViewController
@property(nonatomic, strong) NSMutableArray *questionTypes;
@end
